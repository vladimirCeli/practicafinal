-module(trenes).
-export([buscarVia/1,establecerVia/1, buscarTren/1,generarTrenes/6,unirTrenes/2,controlarPasajeros/3,iniciarCodigo/0, asignarViaTren/2]).  %% public interface of server %%
-import(string, [concat/2]).
%% Send messages to server using name of its process "simple_server" e.g simple_server ! "message"
%===================Metodo de la conexion del servidor================================
%Metodo de la Generacion de Trenes.
generarTrenes(List,Identificador,Estacion, Cargo, NumPasajeros, "estacionado" ) -> List;
generarTrenes(List,Identificador,Estacion, Cargo , NumPasajeros, Estado ) ->
    %io:format("=====Generando tren=========~n"),
    %timer:sleep(10000),
    Valor1="estacionado",
    ValorAux=string:concat(Estado,Valor1),
    Datos = [Identificador,Estacion,Cargo,NumPasajeros,ValorAux],
    ets:insert(tabla_estacionTrenes, {Identificador,Estacion,Cargo,NumPasajeros,ValorAux}),
    %io:format("Arreglo~w~n", [Datos]),
    %io:format("[~w,~w,~w,~w,~w]~n",Datos),
    generarTrenes(Datos,Identificador,Estacion, Cargo, NumPasajeros, "estacionado" ).
%Metodo para la generacion de Vias.
generarViasEstacion(ListAux,IdentificadorVia,"estacionViaRegistrada", EstadoEstacion ) -> ListAux;
generarViasEstacion(ListAux,IdentificadorVia,NombreEstacion, EstadoEstacion) ->
    %io:format("=====Generando Vias en la estaciones=========~n"),
    %timer:sleep(10000),
    %Valor1="Libres",
    ValorAux=string:concat(NombreEstacion,EstadoEstacion),
    Datos= [IdentificadorVia,NombreEstacion,EstadoEstacion,ListAux],
    ets:insert(tabla_viasEstacion, {IdentificadorVia,NombreEstacion,EstadoEstacion,ListAux}),
    %io:format("Arreglo~w~n", [Datos]),
    %io:format("[~w,~w,~w,~w,~w]~n",Datos),
    generarViasEstacion(Datos,IdentificadorVia,"estacionViaRegistrada",EstadoEstacion).
%Metodo para la Unificacion de todos los trenes
unirTrenes(List,List1) ->
    if List == [] ->
        List1;
    true ->
        [H | F] = List,
        %List1 ++ [H],
        unirTrenes(F,List1 ++ [H] )
    end.

%Metodo para controlar el numero de pasajeros de los trenes
%controlarPasajeros(List,ValidadorPasajeros,0) -> Va
iniciarCodigo() ->
    ets:new(tabla_estacionTrenes, [set, public, named_table]),
    %Generacion de todos los codigos
    Tren1=generarTrenes([],1,"NorteSur","urgente",0,""),
    Tren2=generarTrenes([],2,"EsteOeste","normal",0,""),
    Tren3=generarTrenes([],3,"NorteSur","urgente",0,""),
    Tren4=generarTrenes([],4,"EsteOeste","normal",0,""),
    Tren5=generarTrenes([],5,"NorteSur","urgente",0,""),
    Tren6=generarTrenes([],6,"EsteOeste","normal",0,""),
    %Generacion de la Tabla de base de datos para la informacion del las vias de la estacion de trenes.
    ets:new(tabla_viasEstacion, [set, public, named_table]),
    %Estableciendo las vias y estaciones para la base de datos.
    Viaestacion1=generarViasEstacion([],"V1","EsteOeste","Libre"),
    ViaEstacion2=generarViasEstacion([],"V2","NorteSur", "Libre").

controlarPasajeros(Tren,ValidadorPasajeros,Pasajeros) ->
    %Condiciones para validar el numero de pasajeros.
    io:format("=======Controlando el numero de pasajeros=========~n"),
    [{IdentificadorTren,EstacionVia,Preferencia,PasajerosTren,SituacionTren}]= ets:lookup(tabla_estacionTrenes,Tren),
    %Suma de Pasajeros.
    io:format("=======Prueba ~p Pasajeros =========~n",[PasajerosTren]),
    SumatoriaPasajeros=PasajerosTren + Pasajeros,
    if
        SumatoriaPasajeros < 40 ->
            %io:format("PruebaJP: ~p ~n", [Tren]),
            ets:update_element(tabla_estacionTrenes,Tren,{4,SumatoriaPasajeros});
    true ->
        ets:update_element(tabla_estacionTrenes,Tren,{4,40}),
        io:format("=======Tren ~p lleno =========~n",[IdentificadorTren])

    end.
%Metodo para buscar el tren dentro de la BD.
buscarTren(IDENTIFICADORT)->
    Respuesta=ets:lookup(tabla_estacionTrenes, IDENTIFICADORT),
    io:format("Prueba:  ~p~n",Respuesta).


buscarVia(NOMBREVIAESTACION)->
    Respuesta2=ets:lookup(tabla_viasEstacion, NOMBREVIAESTACION),
    io:format("tes:  ~p~n",Respuesta2).
%Metodos para elegir la Via y establecer la prioridad.


%Metodo para buscar la Via almacenada en la base de datos
establecerVia(NOMBREVIAESTACION)->
    Respuesta1=ets:lookup(tabla_viasEstacion, NOMBREVIAESTACION),
    io:format("tes:  ~p~n",Respuesta1).


%Metodo para el registro de trenes a las Vias

asignarViaTren(0,EstacionVia) -> EstacionVia;

asignarViaTren(Tren, EstacionVia) ->
    [{IdentificadorVia,NombreEstacion,EstadoEstacion,ListAux}]= ets:lookup(tabla_viasEstacion, EstacionVia),
    NuevoTren=[ListAux] ++ [Tren],
    ets:update_element(tabla_viasEstacion, EstacionVia,{4, NuevoTren}),
    io:format("tren encontrado ~n"),
    asignarViaTren(0,EstacionVia).
